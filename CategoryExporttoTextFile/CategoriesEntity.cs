﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryExporttoTextFile
{
    public class CategoriesEntity
    {
        public string id { get; set; }
        public string ean { get; set; }
        public string category { get; set; }
        public string type { get; set; }
        public BsonDocument plus { get; set; }
    }
}
