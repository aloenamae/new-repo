﻿using ApplicationStatus;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryExporttoTextFile
{
    class Program
    {
        public static MongoCollection<CategoriesEntity> Category = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/AmazonAsinCategories").GetServer().GetDatabase("AmazonAsinCategories").GetCollection<CategoriesEntity>("categories");

        static void Main(string[] args)
        {
            AppStatus appstatus = new AppStatus("CategoryExporttoTextFile");
            appstatus.Start();
            GetCategories();
            appstatus.Stop();
        }

        public static void GetCategories()
        {
            int count = 1;
            var getcategories = Category.FindAll();

            using (StreamWriter Categories = new StreamWriter("C:\\xampp\\htdocs\\csv\\AmazonAsinCategory.txt"))
            //using (StreamWriter Categories = new StreamWriter("D:\\csvAmazonAsinCategory.txt"))
           
                foreach (var categories in getcategories)
                { 
                    Categories.WriteLine(categories.category + "\t" + categories.ean + "\t" + categories.id);
                    count++;
                    Console.WriteLine(count);
                }
        }


    }
}
